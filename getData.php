<?php 
session_start();
include "inc/config.php";
$stmt = $db->prepare("SELECT * FROM payments INNER JOIN categories ON category = categories.cid WHERE uid = ?");
	$stmt->execute(array($_SESSION['user']));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
 foreach($rows as $r)
 {
     $string = "{\"cols\": [";    
     $column = array_keys($r);
     foreach($column as $key=>$value){
         $cols[]="{id: ".$key.", label: \"".$value."\"}";
     }    
     $string .= implode(",",$cols)."],\"rows\": [";       
   
   $drows[] = "{c:[{v: \"".$r['id']."\"}, {v: \"".$r['amount']."\"}, {v: \"".$r['datetime']."\"}]}, {v: \"".$r['name']."\"}]}";
    }
// you may need to change the above into a function that loops through rows, with $r['id'] etc, referring to the fields you want to inject..
echo $string.implode(",",$drows)."]}";
//print_r($string);



// Instead you can query your database and parse into JSON etc etc

?>
