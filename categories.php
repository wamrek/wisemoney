<?php
session_start();
require_once('inc/config.php');
require_once('inc/header.php');
if (isset($_SESSION['user']))
{
	echo $_SESSION['user'];
	$stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
	$stmt->execute(array($_SESSION['user']));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt2 = $db->prepare("SELECT cid, name, `description` FROM categories WHERE uid = ?");
	$stmt2->execute(array($_SESSION['user']));
	$cats = $stmt2->fetchAll(PDO::FETCH_ASSOC);
	
	$stmt3 = $db->prepare("SELECT SUM(amount) FROM payments WHERE category = ? AND uid = ?"); 
	//print_r($cats);
}
?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header 
        <div class="row">
            <div class="col-lg-12">
                <h1  style="text-align:center; color: #222121; font-family:Tale ; color:#3c8dbc;" >Login</h1>
            </div>
        </div>
        /.row -->

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-6 portfolio-item">
				<div class="bs-callout bs-callout-danger" id="callout-buttons-ie-disabled">
			  </div>
  
	
                <h3 style="text-align:left; font-family:Tale;">
                    Current categories </h3>(<a href="editcat.php">Edit</a>)
                <div class="col-md-6 portfolio-item" style="width:100%">
				<?php
			foreach ($cats as $cat)
			{
				$stmt3->execute(array($cat['cid'], $_SESSION['user']));
				$payments = $stmt3->fetchAll(PDO::FETCH_COLUMN, 0);
			?>
            <h3 style="text-align:left; font-family:Tale;">
                <?= $cat['name'];?>
            </h3>
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">Description: <?= $cat['description']?></p>
                    <p style="float:right; font-size:20px; margin-right:10px;">Payments in this category: <?= $payments[0]; ?></p><br />
                    <p class="offersText" style="font-size:15px;"></p>
                </div>
			</div>
			<?php 
			}
			?>