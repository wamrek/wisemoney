<?php
session_start();
require_once('inc/config.php');
if (isset($_SESSION['user']))
{
	echo $_SESSION['user'];
	$stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
	$stmt->execute(array($_SESSION['user']));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt = $db->prepare("SELECT id, amount, datetime, name FROM payments INNER JOIN categories ON category = categories.cid WHERE uid = ?");
	$stmt->execute(array($_SESSION['user']));
	$pays = $stmt->fetchAll(PDO::FETCH_ASSOC);
	print_r($pays);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <!-- for IE Comptability-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For mobile devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Wise Money">
    <meta name="author" content="MMW">

    <title>Wise Money</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/AdminLTE.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #logo {
            color: #f1c40f;
            float: left;
            height: 50px;
            padding: 15px 15px;
            font-size: 30px;
            line-height: 20px;
            /*text-shadow: -1px -1px 0 #9b59b6, 1px -1px 0 #9b59b6 -1px 1px 0 #9b59b6, 1px 1px 0 #9b59b6*/
        }

        /*#logo:hover{
            color: white ;
        }*/

        .offers{
            margin-left:5px;
            font-family:'Helvetica Neue LT Arabic';
            margin-top:0px;
            padding-bottom:10px;
            padding-top:10px;
        }
        .offersText{
            font-size:20px;
        }
       
    </style>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.min.js"></script>
<script>
$(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  $(this).parents('.form-group').find('button[data-toggle="dropdown"]').html(selText+' <span class="caret"></span>');
});
</script>
</head>

<body>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#686767;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="logo" style="font-family:'Exo BoldItalic';">Wi$e Money</a>
				<h2>Welcome, <?php print_r($rows[0]['username']); ?>!</h2>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" style="color:white;">Settings</a>
                    </li>
                    <li>
                        <a href="#" style="color:white;">Analytics</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header 
        <div class="row">
            <div class="col-lg-12">
                <h1  style="text-align:center; color: #222121; font-family:Tale ; color:#3c8dbc;" >Login</h1>
            </div>
        </div>
        /.row -->

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-6 portfolio-item">
			<?php
				if (isset($_POST['amount']) && isset($_POST['cat']))
				{
					$stmt = $db->prepare("INSERT INTO `payments` (uid, amount, category) VALUES (?, ?, ?)");
					$stmt->execute(array($_SESSION['user'], $_POST['amount'], $_POST['cat']));
					$aff = $stmt->rowCount();	
					//echo $stmt->rowCount();
					if ($aff > 0)
					{
						?>
				<div class="bs-callout bs-callout-danger" id="callout-buttons-ie-disabled">
				<h4>Payment was successful!</h4>
			  </div>
  
			<?php //echo "Payment entered successfully!";
					}	
				}
			?>
                <h3 style="text-align:left; font-family:Tale;">
                    Current payments </h3>
                <div class="col-md-6 portfolio-item" style="width:100%">
				<?php
			foreach ($pays as $pay)
			{
			?>
            <h3 style="text-align:left; font-family:Tale;">
                #<?= $pay['id'];?>
            </h3>
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">$<?= $pay['amount']; ?></p>
                    <p style="float:right; font-size:20px; margin-right:10px;"> <?= $pay['name']; ?></p><br />
                    <p class="offersText" style="font-size:15px;"><?= $pay['datetime']; ?></p>
                </div>
			</div>
			<?php 
			}
			?>