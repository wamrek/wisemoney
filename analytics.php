﻿<?php
session_start();
include "inc/config.php";
?>
<!DOCTYPE html>
<html lang="en">

<html lang="en">

<head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
	
      var jsonData = $.ajax({
          url: "getData.php",
          dataType:"json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
      chart.draw(data, {width: 400, height: 240});
    }

    </script>

    <meta charset="utf-8">
    <!-- for IE Comptability-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For mobile devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Wise Money">
    <meta name="author" content="MMW">

    <title>Wise Money</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/AdminLTE.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #logo {
            color: #f1c40f;
            float: left;
            height: 50px;
            padding: 15px 15px;
            font-size: 30px;
            line-height: 20px;
            /*text-shadow: -1px -1px 0 #9b59b6, 1px -1px 0 #9b59b6 -1px 1px 0 #9b59b6, 1px 1px 0 #9b59b6*/
        }

        /*#logo:hover{
            color: white ;
        }*/

        .offers{
            margin-left:5px;
            font-family:'Helvetica Neue LT Arabic';
            margin-top:0px;
            padding-bottom:10px;
            padding-top:10px;
        }
        .offersText{
            font-size:20px;
        }
       
    </style>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.min.js"></script>
<script>
$(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  $(this).parents('.form-group').find('button[data-toggle="dropdown"]').html(selText+' <span class="caret"></span>');
});
</script>
</head>
<body>
<div id="chart_div"></div>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#686767;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>-->
                <a id="logo" href="userhome.php">Wi$e Money<sub style="text-shadow:none; font-size:medium">wasim</sub></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>-->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <h1 style="text-align:center; color: #222121; font-family:Tale">Analytics</h1>
        </div>
		    <div id="chart_div"></div>

        <div class="box box-primary" style="border-top-color: #e74c3c">

            
        </div><!-- /.box -->

    


        <p style="text-align:center; margin-top:25px; margin-bottom:10px">Copyright &copy; MMW</p>
   
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>