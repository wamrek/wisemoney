﻿<?php
require_once('config.php');

if (isset($_POST['username']))
{
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$stmt = $db->prepare("INSERT INTO users(username, email, password) VALUES (?, ?, ?)");
	$stmt->execute(array($username, $email, $password));
	$aff = $stmt->rowCount();
	if ($aff > 0)
		echo "Registered successfully!";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <!-- for IE Comptability-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For mobile devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Wise Money">
    <meta name="author" content="MMW">

    <title>Wise Money - Sign Up</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    

    <link href="css/AdminLTE.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #logo {
            color: #f1c40f;
            float: left;
            height: 50px;
            padding: 15px 15px;
            font-size: 30px;
            line-height: 20px;
            /*text-shadow: -1px -1px 0 #9b59b6, 1px -1px 0 #9b59b6 -1px 1px 0 #9b59b6, 1px 1px 0 #9b59b6*/
        }


           
    </style>

</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#686767;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="logo" style="font-family:'Exo BoldItalic';">Wi$e Money</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <h1 style="text-align:center; color: #222121; font-family:Tale">Sign Up</h1>
        </div>
        <div class="box box-primary">

            <!-- form start -->
            <form role="form" action="" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="username">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="rePassword">ReEnter Password</label>
                        <input type="password" class="form-control" id="rePassword" placeholder="ReEnter Password">
                    </div>
                    <div class="form-group">
                        <label for="dropdownmenu">Gender</label>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-expanded="true">
                                Select
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Male</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Female</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <label for="age">Age</label>
                            <input type="text" class="form-control" id="age" placeholder="Age">
                        </div>
                    </div>
                    <div class="form-group">
                        <div>
                            <label for="captcha">Are you a Human?</label><br />
                            <input type="text" class="form-control" id="captcha" placeholder="Enter what you see" style="width:70%; display:inline"> <wbr />
                            <span style="width:30px; height:10px;">
                                <img src="image/input-black.gif" width="150px" height="50px">
                            </span>
                    </div>
                    <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="float:left">Submit</button>
                            <button type="button" class="btn btn-danger"  style="margin-left:15px;">Reset</button>
                    </div>
                        

                    </div>
                </div><!-- /.box-body -->
            </form>
            
        </div><!-- /.box -->

    </div>


        <p style="text-align:center; margin-top:25px; margin-bottom:10px">Copyright &copy; MMW</p>
   
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>