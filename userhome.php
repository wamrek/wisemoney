﻿<?php
session_start();
require_once('inc/config.php');
require_once('inc/header.php');
if (isset($_SESSION['user']))
{
	echo $_SESSION['user'];
	$stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
	$stmt->execute(array($_SESSION['user']));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt = $db->prepare("SELECT cid, name FROM `categories` WHERE uid = ?");
	$stmt->execute(array($_SESSION['user']));
	$cats = $stmt->fetchAll(PDO::FETCH_ASSOC);
}



?>
        <div class="row">
            <div class="col-md-6 portfolio-item">
			<?php
				if (isset($_POST['amount']) && isset($_POST['cat']))
				{
					$stmt = $db->prepare("INSERT INTO `payments` (uid, amount, category) VALUES (?, ?, ?)");
					$stmt->execute(array($_SESSION['user'], $_POST['amount'], $_POST['cat']));
					$aff = $stmt->rowCount();	
					//echo $stmt->rowCount();
					if ($aff > 0)
					{
						?>
				<div class="bs-callout bs-callout-danger" id="callout-buttons-ie-disabled">
				<h4>Payment was successful!</h4>
			  </div>
  
			<?php //echo "Payment entered successfully!";
					}	
				}
			?>
                <h3 style="text-align:left; font-family:Tale;">
                    Add Payment
                </h3>
                <div class="box box-primary">
                   <form role="form" method="post" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="text" name="amount" class="form-control" id="amount" placeholder="Enter Amount" style="width:70%"> <wbr />
                            </div>
                            <div class="form-group">
                                
                                <input type="text" class="form-control" id="note" placeholder="Enter a note">
                            
                                                 
                                    <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Travelling</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Food</a></li>-->
									Category: <select name="cat">
									<?php foreach ($cats as $cat)
									{
										echo "<option value=\"".$cat['cid']."\">".$cat['name']."</option>";
									}
									?>
									</select>
									<br><a href="editcat.php">Edit</a><br>

                           

                            <input type="submit" value="Submit" class="btn" href="#" style=" background-color:#2ecc71; color:white; margin-top:10px;">
                           
                           
                        </div><!-- /.box-body -->
                       
                    </form>
                </div><!-- /.box -->
               

            </div>
            <div class="col-md-6 portfolio-item">
                <h3 style="text-align:left; font-family:Tale;">
                    Search your history
                </h3>
                <div class="box box-primary">

                    <!-- form start -->
                    <form role="form">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="text" class="form-control" id="search" placeholder="Enter date or note">
                            </div>
                            <a class="btn" href="#" style=" background-color:#2ecc71; color:white;">Search</a>

                        </div><!-- /.box-body -->
                       
                    </form>
                </div><!-- /.box -->
               

        </div>
            </div>
        <div class="col-md-6 portfolio-item" style="width:100%">
            <h3 style="text-align:left; font-family:Tale;">
                Nearby
            </h3>
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">2 Pepsi + 1 for Free</p>
                    <p style="float:right; font-size:20px; margin-right:10px;"> Carrefour</p><br />
                    <p class="offersText" style="font-size:15px;"> 15 mins ago</p>
                </div>
            </div><!-- /.box -->
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">Sony Bravia LCD</p>
                    <p style="float:right; font-size:20px; margin-right:10px;"> Ragab Sons</p><br />
                    <p class="offersText" style="font-size:15px;"> 1 day ago</p>
                </div>
            </div>


        </div>
        <div class="col-md-6 portfolio-item" style="width:100%">
            <h3 style="text-align:left; font-family:Tale;">
                Exclusive Offers
            </h3>
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">2 Pepsi + 1 for Free</p>
                    <p style="float:right; font-size:20px; margin-right:10px;"> Carrefour</p><br />
                    <p class="offersText" style="font-size:15px;"> 15 mins ago</p>
                    <p class="offersText" style="font-size:15px;"> Code: 5xx32</p>
                </div>
            </div><!-- /.box -->
            <div class="box box-primary">
                <div class="offers">
                    <p class="offersText" style="display:inline;">Sony Bravia LCD</p>
                    <p style="float:right; font-size:20px; margin-right:10px;"> Ragab Sons</p><br />
                    <p class="offersText" style="font-size:15px;"> 1 day ago</p>
                    <p class="offersText" style="font-size:15px;"> Code: 95x32</p>
                </div>
            </div>


        <?php
		require_once('inc/footer.php');
		?>
