﻿<?php
require_once('config.php');
?>
<?php
session_start();
if ((isset($_POST['email']) && isset($_POST['password'])))
{
	$u = $_POST['email'];
	$p = $_POST['password'];

	$stmt = $db->prepare("SELECT id FROM users WHERE `email` = ? AND `password` = ?");
	$stmt->execute(array($u, $p));
	$rows = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
	$aff = $stmt->rowCount();
	
	if ($aff > 0)
	{
		$_SESSION['user'] = $rows[0];
		header("Location: userhome.php");
		print_r($rows);
		print_r($_SESSION);
	}
	else
	{
		echo "Wrong email or password!";
	}
}
else if (isset($_POST['pemail']) && isset($_POST['ppassword']))
{
}
else{
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <!-- for IE Comptability-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For mobile devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Wise Money">
    <meta name="author" content="MMW">

    <title>Wise Money</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/AdminLTE.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/2-col-portfolio.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        #logo {
            color: #f1c40f;
            float: left;
            height: 50px;
            padding: 15px 15px;
            font-size: 30px;
            line-height: 20px;
            /*text-shadow: -1px -1px 0 #9b59b6, 1px -1px 0 #9b59b6 -1px 1px 0 #9b59b6, 1px 1px 0 #9b59b6*/
        }

        /*#logo:hover{
            color: white ;
        }*/
    </style>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#686767;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="logo" style="font-family:'Exo BoldItalic';">Wi$e Money</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1  style="text-align:center; color: #222121; font-family:Tale ; color:#3c8dbc;" >Login</h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-6 portfolio-item">
                <h3 style="text-align:center">
                    As a User
                </h3>
                <div class="box box-primary">
                    
                    <!-- form start -->
                    <form role="form" action="" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                            </div>
                           
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Me!
                                </label>
  								<input type="submit" value="Login" style="background-color:#2ecc71; color:white;margin-left:10px; margin-right:10px;">Login
								<!--<a class="btn" href="#" style=" background-color:#2ecc71; color:white;margin-left:10px; margin-right:10px;">Login</a>-->
                                <a class="btn" href="usersignup.php" style="float:right; background-color:#1774aa; color:white;">Sign Up</a>
                                

                            </div>
                        </div><!-- /.box-body -->
                       
                    </form>
                </div><!-- /.box -->
               

            </div>
            <div class="col-md-6 portfolio-item">
                <h3 style="text-align:center">
                    As a Place
                </h3>
                <div class="box box-primary" style="border-top-color: #2ecc71">

                    <!-- form start -->
                    <form role="form" action="" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="pemail">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="ppassword">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Me!
                                </label> 
                                <!--<a class="btn" href="#" style=" background-color:#2ecc71; color:white;margin-left:10px; margin-right:10px;">Login</a>-->
								<input type="submit" value="Login" style="background-color:#2ecc71; color:white;margin-left:10px; margin-right:10px;">
                                <a class="btn" href="Place SignUp.html" style="float:right; background-color:#1774aa; color:white;">Sign Up</a>

                            </div>
                        </div><!-- /.box-body -->
                       
                    </form>
                </div><!-- /.box -->
               

        </div>
            </div>
        <div class="container" style="padding:0px; margin:0px;">
                <div class="box box-primary" style="padding:0px; margin:0px; border-top:0px;">
                    <h1  style=" font-family:Tale; text-align:center; padding:0px; margin:0px; color: #3c8dbc" >
                        About Us
                    </h1> <br />

                    <p style="text-align:center; text-shadow:2px ; font-family:'Helvetica Neue LT Arabic' ; padding-bottom: 10px;">
                        This application aims to be an easy to use intelligent system that helps the user manage his financial resources, by analyzing income and payments. Being accessible through different platforms (web-based, mobile and tablets). With wide range of features (different analytics, easy to use system being concerned with User Experience principles and Smooth GUI).
                        Creating a Win-Win situation to both users and Places. Providing users with Wallet Friendly offers from places (and special ones for app users), and providing places with useful users’ Database which can offer lots of useful information.

                    </p>
                </div>
        </div>

        <p style="text-align:center; margin-top:18px; margin-bottom:15px;">Copyright &copy; MMW</p>      
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
<?php 

}
?>